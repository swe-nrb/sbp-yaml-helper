import { Component, OnInit, ViewEncapsulation, Input, OnChanges, Renderer } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';

import YAML from 'yamljs';

@Component({
  selector: 'app-json-preview',
  templateUrl: './json-preview.component.html',
  styleUrls: ['./json-preview.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class JsonPreviewComponent implements OnInit, OnChanges {
  @Input('json-data') jsonObj: any;
  output: string;
  constructor(private clipboard: ClipboardService, private renderer: Renderer) {}

  ngOnInit() {}

  ngOnChanges() {
    this.output = YAML.stringify(this.jsonObj, 99, 2);
  }

  copy() {
    this.clipboard.copyFromContent(this.output, this.renderer);
  }
}
