import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';

import { Language, ThingLanguage } from '../process-form.component';

@Component({
  selector: 'app-language-block',
  templateUrl: './language-block.component.html',
  styleUrls: ['./language-block.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LanguageBlockComponent implements OnInit {
  @Input() language: Language;
  @Output() update: EventEmitter<any>;
  data: ThingLanguage;
  constructor() {
    this.data = {
      name: '',
    };
    this.update = new EventEmitter();
  }

  ngOnInit() {
    this.updateLang();
  }

  deleteLang() {
    this.update.emit({ language: this.language, data: undefined });
  }
  
  updateLang() {
    let d = _.cloneDeep(this.data);
    this.update.emit({ language: this.language, data: d });
  }

}
