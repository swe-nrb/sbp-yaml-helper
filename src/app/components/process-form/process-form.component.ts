import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import * as slug from 'slug';
import * as _ from 'lodash';
import { UUID } from 'angular2-uuid';

@Component({
  selector: 'app-process-form',
  templateUrl: './process-form.component.html',
  styleUrls: ['./process-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProcessFormComponent implements OnInit {
  @Output() output = new EventEmitter();
  thing: Thing;
  languages: Language[]
  selectedLanguage: Language
  pickedLanguages: Language[]
  constructor() {
    // list possible languages
    this.languages = [{
      code: 'en',
      name: 'Engelska'
    }, {
      code: 'de',
      name: 'Tyska'
    }, {
      code: 'fr',
      name: 'Franska'
    }, {
      code: 'dk',
      name: 'Danska'
    }, {
      code: 'no',
      name: 'Norska'
    }, {
      code: 'fi',
      name: 'Finska'
    }];
    // list picked languages
    this.pickedLanguages = [{
      code: 'sv',
      name: 'Svenska'
    }];
    // create thing
    this.thing = {
      _v: 1,
      id: '',
      slug: '',
      filename: '',
      order: 1,
      parent: '',
      source: '',
      process: {
        sv: {
          name: ''
        }
      }
    }
  }

  ngOnInit() {
    // send on init just to display something
    this.output.emit(this.thing);
  }

  submit() {
    this.thing.id = UUID.UUID();
    // update filename
    let name;
    if(this.thing.parent.length > 0) {
      name = `${this.thing.parent}-${this.thing.process.sv.name}`;
    } else {
      name = `${this.thing.source}-${this.thing.parent}-${this.thing.process.sv.name}`;
    }
    this.thing.slug = slug(name).toLowerCase();
    this.thing.filename = `${this.thing.slug}.yml`;
    this.output.emit(this.thing);
  }

  addLanguage() {
    if(this.selectedLanguage) {
      this.pickedLanguages.push(this.selectedLanguage);
    }
  }

  /**
   * This function listens to the language blocks and updated thing accordinly
   * @param language 
   */
  languageUpdated(language) {
    if(language.data == undefined ) {
      delete this.thing.process[language.language.code];
      let lang = _.find(this.pickedLanguages, { code: language.language.code });
      if(lang) {
        this.pickedLanguages.splice(this.pickedLanguages.indexOf(lang), 1);
      }
    } else {
      this.thing.process[language.language.code] = language.data;
    }
    this.submit();
  }

}

export interface Language {
  code: string
  name: string
}

export interface Thing {
  _v: number
  id: string
  slug: string
  filename: string
  order: number
  parent: string
  source: string
  process: {
    sv: ThingLanguage
    en?: ThingLanguage
  }
}

export interface ThingLanguage {
  name: string
}
