import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-list-templates',
  templateUrl: './list-templates.component.html',
  styleUrls: ['./list-templates.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListTemplatesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
