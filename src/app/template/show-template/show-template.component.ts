import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-show-template',
  templateUrl: './show-template.component.html',
  styleUrls: ['./show-template.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ShowTemplateComponent implements OnInit {
  output: any;
  constructor() {}

  ngOnInit() {
  }

  receiveJson(jsonObj) {
    this.output = _.cloneDeep(jsonObj);
  }

}
