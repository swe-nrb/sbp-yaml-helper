import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListTemplatesComponent} from './list-templates/list-templates.component';
import { ShowTemplateComponent } from './show-template/show-template.component';

const routes: Routes = [{
  path: 'templates',
  component: ListTemplatesComponent
}, {
  path: 'templates/:templateId',
  component: ShowTemplateComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateRoutingModule { }
