import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';

import { TemplateRoutingModule } from './template-routing.module';
import { ListTemplatesComponent } from './list-templates/list-templates.component';
import { ShowTemplateComponent } from './show-template/show-template.component';

import { JsonPreviewComponent } from '../components/json-preview/json-preview.component';
import { ProcessFormComponent } from '../components/process-form/process-form.component';
import { LanguageBlockComponent } from '../components/process-form/language-block/language-block.component';

@NgModule({
  imports: [
    CommonModule,
    TemplateRoutingModule,
    FormsModule,
    ClipboardModule
  ],
  declarations: [
    ListTemplatesComponent,
    ShowTemplateComponent,
    JsonPreviewComponent,
    ProcessFormComponent,
    LanguageBlockComponent
  ]
})
export class TemplateModule { }
